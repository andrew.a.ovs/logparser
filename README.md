logparser app is used to parse Apache Httpd log files and store obtained results to MongoDB

to build this app you need JDK 1.8, Scala 2.12.7 and sbt 1.2.4

to create an executable fat-JAR use sbt assembly command,
after successful assembly you will find assembly JAR(logparser_aovs_assembly.jar) under target/

to run assembly JAR use 
java -jar logparser_aovs_assembly.jar -f (path to your file)


In order to use this app you have to run MongoDB either on default path(mongodb://localhost:27017),
or provide your custom path to running Mongo instance with -mongo command-line option.

Path to file being parsed is specified with -f option, which is mandatory.

Command-line options list:

-f (filepath) : filepath to file, mandatory

-mongo (mongo connection path) : path to running mongo instance, optional

-format (log format) : Apache log file format, optional

-v : shows app version, optional

-h : shows help prompt, optional