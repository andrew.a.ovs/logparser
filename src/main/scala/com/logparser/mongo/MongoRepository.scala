package com.logparser.mongo

import com.logparser.Repository
import com.logparser.domain._
import com.typesafe.config.ConfigFactory
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala.bson.BsonDocument
import org.mongodb.scala.bson.codecs.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
import org.mongodb.scala.{MongoClient, MongoCollection}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class MongoRepository(connectionString: String = ConfigFactory.load().getString("defaultMongoPath")) extends Repository {

  private val mongoClient: MongoClient = MongoClient(connectionString)

  private val codecRegistry = fromRegistries(fromProviders(
    classOf[LogEntry],
    classOf[BrowserMetrics], classOf[CombinedBrowserMetrics],
    classOf[ChannelMetrics], classOf[CombinedChannelMetrics],
    classOf[OSMetrics], classOf[CombinedOSMetrics]
  ), DEFAULT_CODEC_REGISTRY)

  private val database = mongoClient.getDatabase(ConfigFactory.load().getString("defaultMongoDatabaseName")).withCodecRegistry(codecRegistry)

  private val entriesCollection: MongoCollection[LogEntry] = database.getCollection(ConfigFactory.load().getString("defaultLogEntriesCollectionName"))

  private val browserMetricsCollection: MongoCollection[CombinedBrowserMetrics] = database.getCollection(ConfigFactory.load().getString("defaultBrowserMetricsCollectionName"))

  private val channelMetricsCollection: MongoCollection[CombinedChannelMetrics] = database.getCollection(ConfigFactory.load().getString("defaultChannelMetricsCollectionName"))

  private val osMetricsCollection: MongoCollection[CombinedOSMetrics] = database.getCollection(ConfigFactory.load().getString("defaultOSMetricsCollectionName"))

  override def insertLogs(iterable: Iterable[LogEntry]): Future[Any] = entriesCollection.insertMany(iterable.toSeq).toFuture()

  override def readLogs(): Future[Seq[LogEntry]] = entriesCollection.find().toFuture()

  override def updateBrowserMetrics(newBrowserMetrics: CombinedBrowserMetrics): Future[Object] = {
    browserMetricsCollection.find().headOption().flatMap{
      case None => browserMetricsCollection.insertOne(newBrowserMetrics).toFuture()
      case Some(existingBrowserMetrics) => browserMetricsCollection.replaceOne(new BsonDocument(), newBrowserMetrics merge existingBrowserMetrics).toFuture()
    }
  }

  override def updateChannelMetrics(newChannelMetrics: CombinedChannelMetrics): Future[Object] = {
    channelMetricsCollection.find().headOption().flatMap{
      case None => channelMetricsCollection.insertOne(newChannelMetrics).toFuture()
      case Some(existingChannelMetrics) => channelMetricsCollection.replaceOne(new BsonDocument(), newChannelMetrics merge existingChannelMetrics).toFuture()
    }
  }

  override def updateOSMetrics(newOSMetrics: CombinedOSMetrics): Future[Object] = {
    osMetricsCollection.find().headOption().flatMap{
      case None => osMetricsCollection.insertOne(newOSMetrics).toFuture()
      case Some(existingOSMetrics) => osMetricsCollection.replaceOne(new BsonDocument(), newOSMetrics merge existingOSMetrics).toFuture()
    }
  }
}
