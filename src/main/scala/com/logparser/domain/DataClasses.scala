package com.logparser.domain

import java.util.Date


case class LogEntry(hostIp: String, timestamp: Long, date: Date, httpMethod: String, responseStatus: Int, channel: String, browser: String, os: String)

case class BrowserMetrics(name: String, absoluteCount: Long, percentage: Double)

case class CombinedBrowserMetrics(totalCount: Long, browsers: List[BrowserMetrics]){
  def merge(that: CombinedBrowserMetrics): CombinedBrowserMetrics = {
    val resultingTotalCount = this.totalCount + that.totalCount
    CombinedBrowserMetrics(
      totalCount = resultingTotalCount,
      browsers = (this.browsers ++ that.browsers)
        .groupBy(_.name)
        .mapValues(_.map(_.absoluteCount).sum)
        .map{case (name, count) => BrowserMetrics(name, count, count.toDouble * 100 / resultingTotalCount)}
        .toList
    )
  }
}

case class ChannelMetrics(name: String, absoluteCount: Long, percentage: Double)

case class CombinedChannelMetrics(totalCount: Long, channels: List[ChannelMetrics]){
  def merge(that: CombinedChannelMetrics): CombinedChannelMetrics = {
    val resultingTotalCount = this.totalCount + that.totalCount
    CombinedChannelMetrics(
      totalCount = resultingTotalCount,
      channels = (this.channels ++ that.channels)
        .groupBy(_.name)
        .mapValues(_.map(_.absoluteCount).sum)
        .map{case (name, count) => ChannelMetrics(name, count, count.toDouble * 100 / resultingTotalCount )}
        .toList
    )
  }
}

case class OSMetrics(name: String, absoluteCount: Long, percentage: Double)

case class CombinedOSMetrics(totalCount: Long, oss: List[OSMetrics]) {
  def merge(that: CombinedOSMetrics): CombinedOSMetrics = {
    val resultingTotalCount = this.totalCount + that.totalCount
    CombinedOSMetrics(
      totalCount = resultingTotalCount,
      oss = (this.oss ++ that.oss)
        .groupBy(_.name)
        .mapValues(_.map(_.absoluteCount).sum)
        .map{case (name, count) => OSMetrics(name, count, count.toDouble * 100 / resultingTotalCount)}
        .toList
    )
  }
}

case class Metrics(entries: List[LogEntry], browsersMetrics: CombinedBrowserMetrics, channelsMetrics: CombinedChannelMetrics, ossMetrics: CombinedOSMetrics)
