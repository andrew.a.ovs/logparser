package com.logparser

object ParserUtils {
  def parseBrowser(userAgent: String): String = Option(userAgent).filter(ua => !ua.isEmpty && ua != "-" && ua != "\"-\"").map{
    case ua if ua.contains("Edge") => "EDGE"
    case ua if ua.contains("MSIE") => "INTERNET_EXPLORER"
    case ua if ua.contains("Seamonkey") => "SEAMONKEY"
    case ua if ua.contains("Chromium") => "CHROMIUM"
    case ua if ua.contains("OPR") || ua.contains("Opera") => "OPERA"
    case ua if ua.contains("Firefox") && !ua.contains("Seamonkey") => "FIREFOX"
    case ua if ua.contains("Safari") && !ua.contains("Chrome") && !ua.contains("Chromium") => "SAFARI"
    case ua if ua.contains("Chrome") && !ua.contains("Chromium") => "CHROME"
    case _ => "OTHER_BROWSER"
  } getOrElse "UNDEFINED_BROWSER"

  def parseChannel(userAgent: String): String = Option(userAgent).filter(ua => !ua.isEmpty && ua != "-" && ua != "\"-\"").map{
    case s if s.contains("Mobi") => "MOBILE"
    case _ => "DESKTOP"
  } getOrElse "UNDEFINED_CHANNEL"

  def parseHttpMethod(request: String): String = Option(request).filter(req => !req.isEmpty && req != "-" && req != "\"-\"").map{
    case req if req.contains("GET") => "GET"
    case req if req.contains("POST") => "POST"
    case req if req.contains("PUT") => "PUT"
    case req if req.contains("DELETE") => "DELETE"
    case req if req.contains("HEAD") => "HEAD"
    case req if req.contains("OPTION") => "OPTION"
    case req if req.contains("PATCH") => "PATCH"
    case _ => "UNDEFINED_METHOD"
  } getOrElse "UNDEFINED_METHOD"

  def parseOS(userAgent: String): String = Option(userAgent).filter(ua => !ua.isEmpty && ua != "-" && ua != "\"-\"").map{
    case ua if ua.contains("Windows Phone") => "WINDOWS_PHONE"
    case ua if ua.contains("Android") => "ANDROID"
    case ua if ua.contains("Windows") => "WINDOWS"
    case ua if ua.contains("iPhone") || ua.contains("iPad") => "IOS"
    case ua if ua.contains("Macintosh") => "OSX"
    case ua if ua.contains("Linux") || ua.contains("CentOS") => "LINUX"
    case _ => "OTHER_OS"
  } getOrElse "UNDEFINED_OS"
}
