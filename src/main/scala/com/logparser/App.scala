package com.logparser

import com.logparser.impl.CombinedParser
import com.logparser.mongo.MongoRepository
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.LazyLogging

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success, Try}

object App extends LazyLogging {

  case class ArgumentsParsingResult(filepath: String, parser: Parser, repo: Repository, logMessages: Array[String])

  def main(args: Array[String]): Unit = {
    parseCommandLineArgs(args) match {
      case Left(errorMessage) =>
        logger.error(errorMessage)
        return
      case Right(ArgumentsParsingResult(filepath, parser, mongoRepository, logMessages)) =>
        logMessages.foreach(m => logger.info(m))
        parser.parse(filepath) match {
          case Failure(exception) => logger.error(s"File: $filepath failed with exception: $exception")
          case Success(result) =>
            logger.info(s"Successfully parsed ${result.entries.size} log entries, now inserting to DB")
            mongoRepository.insertLogs(result.entries).onComplete{
              case Success(_) => logger.info(s"Successfully inserted ${result.entries.size} log entries to DB")
              case Failure(exception) => logger.error(s"Failed to insert ${result.entries.size} log entries to DB", exception)
            }
            mongoRepository.updateBrowserMetrics(result.browsersMetrics).onComplete{
              case Success(_) => logger.info(s"Successfully updated browser metrics")
              case Failure(exception) => logger.error(s"Failed to update browser metrics", exception)
            }
            mongoRepository.updateChannelMetrics(result.channelsMetrics).onComplete{
              case Success(_) => logger.info(s"Successfully updated channels metrics")
              case Failure(exception) => logger.error(s"Failed to update channels metrics", exception)
            }
            mongoRepository.updateOSMetrics(result.ossMetrics).onComplete{
              case Success(_) => logger.info(s"Successfully updated OS metrics")
              case Failure(exception) => logger.error(s"Failed to update OS metrics", exception)
            }
        }
    }
    scala.io.StdIn.readLine("\nPress any key to exit\n")
  }

  private def parseCommandLineArgs(args: Array[String]): Either[String, ArgumentsParsingResult] = {
    val config = ConfigFactory.load

    val logMessages = ArrayBuffer[String]()

    if(args.contains("-v")) logMessages += s"App version: ${ConfigFactory.load.getString("version")}"

    if(args.contains("-h")) logMessages += ConfigFactory.load.getString("helpPrompt")

    val filepath = args.find(_ == "-f") match {
      case None => Left(config.getString("provideFilePathErrorMessage"))
      case Some(_) =>
        if(args.indexOf("-f") != (args.length - 1)) Right(args(args.indexOf("-f") + 1))
        else Left(config.getString("provideFilePathErrorMessage"))
    }
    if(filepath.isLeft) return Left(filepath.left.get)

    val parser = args.find(_ == "-format") match {
      case None => Right(new CombinedParser)
      case Some(_) =>
        if(args.indexOf("-format") != (args.length - 1)) {
          val providedFormat = args(args.indexOf("-format") + 1)
          providedFormat match {
            case "combined" => Right(new CombinedParser)
            case _ => Left(s"No implementation was found for specified log format: $providedFormat")
          }
        }
        else Right(new CombinedParser)
    }
    if(parser.isLeft) return Left(parser.left.get)

    val repository = config.getString("storageType") match {
      case "mongodb" =>
        args.find(_ == "-mongo") match {
          case None => Try(Right(new MongoRepository)).getOrElse(Left(s"Could not connect to Mongo using default path: ${ConfigFactory.load.getString("defaultMongoPath")}"))
          case Some(_) =>
            if(args.indexOf("-mongo") != (args.length - 1)) Try(Right(new MongoRepository(args(args.indexOf("-mongo") + 1)))).getOrElse(Left(s"Could not connect to Mongo using provided path: ${args(args.indexOf("-mongo") + 1)}"))
            else Try(Right(new MongoRepository)).getOrElse(Left(s"Could not connect to Mongo using default path: ${ConfigFactory.load.getString("defaultMongoPath")}"))
        }
      case _ => Left(s"Storage type ${config.getString("storageType")} is not supported")
    }
    if(repository.isLeft) return Left(repository.left.get)

    Right(ArgumentsParsingResult(filepath.right.get, parser.right.get, repository.right.get, logMessages.toArray))
  }
}
