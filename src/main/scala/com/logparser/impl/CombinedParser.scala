package com.logparser.impl

import java.util.{Date, StringTokenizer}

import com.logparser.Parser
import com.logparser.ParserUtils._
import com.logparser.domain._

import scala.util.{Failure, Success, Try}

class CombinedParser extends Parser {

  //Made public in implementation for testing purposes
  override def parseLogLine(s: String): Either[(String, Throwable), LogEntry] = {
    Try{
      val (host, date, request, responseCode, userAgent) = tokenizeLogLineString(s)
      val (timestamp, dt) = parseDate(date)

      LogEntry(
        host, timestamp, dt, parseHttpMethod(request), responseCode,
        parseChannel(userAgent), parseBrowser(userAgent), parseOS(userAgent)
      )
    } match {
      case Success(logEntry) => Right(logEntry)
      case Failure(exc) => Left((s, exc))
    }
  }

  private def tokenizeLogLineString(s: String): (String, String, String, Int, String) = {
    val delim = "\\[\\]\""
    val tokenizer = new StringTokenizer(s, delim)
    val host = tokenizer.nextToken(" ")
    tokenizer.nextToken(delim)
    val date = tokenizer.nextToken()
    tokenizer.nextToken()
    val request = tokenizer.nextToken()
    val responseCode = tokenizer.nextToken().trim.split(" ")(0)
    tokenizer.nextToken()
    tokenizer.nextToken()
    val userAgent = tokenizer.nextToken()
    (host, date, request, responseCode.toInt, userAgent)
  }

  //Made public in implementation for testing purposes
  override def parseDate(date: String): (Long, Date) = super.parseDate(date)
}
