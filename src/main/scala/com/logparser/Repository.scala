package com.logparser

import com.logparser.domain.{CombinedBrowserMetrics, CombinedChannelMetrics, CombinedOSMetrics, LogEntry}
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.Future

trait Repository extends LazyLogging {
  def insertLogs(iterable: Iterable[LogEntry]): Future[Any]

  def readLogs(): Future[Iterable[LogEntry]]

  def updateBrowserMetrics(newBrowserMetrics: CombinedBrowserMetrics): Future[Any]

  def updateChannelMetrics(newChannelMetrics: CombinedChannelMetrics): Future[Any]

  def updateOSMetrics(newOSMetrics: CombinedOSMetrics): Future[Any]
}
