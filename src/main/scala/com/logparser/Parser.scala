package com.logparser

import java.io.File
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.util.Date

import com.logparser.domain._
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.LazyLogging

import scala.io.Source
import scala.util.Try

trait Parser extends LazyLogging {

  private val DATE_FORMATTER = DateTimeFormatter.ofPattern(ConfigFactory.load().getString("dateFormat"))

  protected def parseLogLine(s: String): Either[(String, Throwable), LogEntry]

  def parse(filePath: String): Try[Metrics] = {
    Try(
      Source.fromFile(new File(filePath)).getLines().toList
        .map(parseLogLine)
        .partition(_.isRight) match {
        case (successes, failures) =>
          logFailures(failures.map{case Left(errorTuple) => errorTuple})
          val entries = successes.map{case Right(logEntry) => logEntry}
          val totalSize = entries.size
          Metrics(
            entries,
            browsersMetrics = entries.groupBy(_.browser).mapValues(_.size).map{case (name, count) => BrowserMetrics(name, count, count.toDouble * 100 / totalSize )}.toList.foldLeft(CombinedBrowserMetrics(0, Nil)){
              case (combMet, met) => CombinedBrowserMetrics(combMet.totalCount + met.absoluteCount, met :: combMet.browsers)
            },
            channelsMetrics = entries.groupBy(_.channel).mapValues(_.size).map{case (name, count) => ChannelMetrics(name, count, count.toDouble * 100 / totalSize)}.toList.foldLeft(CombinedChannelMetrics(0, Nil)){
              case (combMet, met) => CombinedChannelMetrics(combMet.totalCount + met.absoluteCount, met :: combMet.channels)
            },
            ossMetrics = entries.groupBy(_.os).mapValues(_.size).map{case (name, count) => OSMetrics(name, count, count.toDouble * 100 / totalSize)}.toList.foldLeft(CombinedOSMetrics(0, Nil)){
              case (combMet, met) => CombinedOSMetrics(combMet.totalCount + met.absoluteCount, met :: combMet.oss)
            }
          )
       }
    )
  }

  protected def parseDate(date: String): (Long, Date) = {
    val dt = Date.from(ZonedDateTime.parse(date.replace("[", "").replace("]", ""), DATE_FORMATTER).toInstant)
    (dt.getTime, dt)
  }

  protected def logFailures(failures: List[(String, Throwable)]): Unit = {
    failures.foreach{
      case (line, exception) => logger.error(s"Could not parse line: $line due to exception: ${exception.toString}")
    }
  }
}
