package com.logparser.impl

import java.time.{Month, ZoneId, ZoneOffset, ZonedDateTime}
import java.util.Date

import com.logparser.domain.LogEntry
import org.junit.Assert._
import org.scalatest.FunSuite

class CombinedParserSuite extends FunSuite {

  private val parser = new CombinedParser

  test("should correctly parse dates"){
    val zdt1 = Date.from(ZonedDateTime.of(
      2018, Month.SEPTEMBER.getValue, 30, 3, 42, 9, 0,
      ZoneId.ofOffset("", ZoneOffset.of("+0300"))
    ).toInstant)
    assertEquals(parser.parseDate("[30/Sep/2018:03:42:09 +0300]"), (zdt1.toInstant.toEpochMilli, zdt1))

    val zdt2 = Date.from(ZonedDateTime.of(
      2018, Month.OCTOBER.getValue, 1, 19, 2, 56, 0,
      ZoneId.ofOffset("", ZoneOffset.of("+0300"))
    ).toInstant)
    assertEquals(parser.parseDate("[01/Oct/2018:19:02:56 +0300]"), (zdt2.toInstant.toEpochMilli, zdt2))

    val zdt3 = Date.from(ZonedDateTime.of(
      2018, Month.OCTOBER.getValue, 6, 18, 51, 6, 0,
      ZoneId.ofOffset("", ZoneOffset.of("+0300"))
    ).toInstant)
    assertEquals(parser.parseDate("[06/Oct/2018:18:51:06 +0300]"), (zdt3.toInstant.toEpochMilli, zdt3))
  }

  test("should correctly parse log line"){
    val zdt1 = Date.from(ZonedDateTime.of(
      2018, Month.SEPTEMBER.getValue, 30, 3, 42, 9, 0,
      ZoneId.ofOffset("", ZoneOffset.of("+0300"))
    ).toInstant)
    val parseResult1 = parser.parseLogLine(
      "194.58.107.246 - - [30/Sep/2018:03:42:09 +0300] \"GET /console HTTP/1.1\" 404 205 \"-\" \"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0\""
    ).right.get
    assertEquals(
      parseResult1,
      LogEntry("194.58.107.246", zdt1.toInstant.toEpochMilli, zdt1, "GET", 404, "DESKTOP", "FIREFOX", "WINDOWS")
    )

    val zdt2 = Date.from(ZonedDateTime.of(
      2018, Month.OCTOBER.getValue, 1, 18, 24, 6, 0,
      ZoneId.ofOffset("", ZoneOffset.of("+0300"))
    ).toInstant)
    val parseResult2 = parser.parseLogLine(
      "109.86.214.4 - - [01/Oct/2018:18:24:06 +0300] \"GET /ojs/lib/pkp/js/classes/ObjectProxy.js HTTP/1.1\" 200 4591 \"http://nti.khai.edu/ojs/\" \"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36\""
    ).right.get
    assertEquals(
      parseResult2,
      LogEntry("109.86.214.4", zdt2.toInstant.toEpochMilli, zdt2, "GET", 200, "DESKTOP", "CHROME", "WINDOWS")
    )
  }
}
