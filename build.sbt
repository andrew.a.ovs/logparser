name := "logparser"

version := "1.0"

scalaVersion := "2.12.7"

libraryDependencies ++= Seq(
  "org.mongodb.scala" %% "mongo-scala-driver" % "2.4.2",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.0",
  "org.slf4j" % "slf4j-simple" % "1.7.25",
  "junit" % "junit" % "4.12" % Test,
  "org.scalatest" %% "scalatest" % "3.0.1",
  "com.typesafe" % "config" % "1.3.3"
)

mainClass in assembly := some("com.logparser.App")

assemblyJarName := "logparser_aovs_assembly.jar"

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", "MANIFEST.MF") => MergeStrategy.discard
  case _ => MergeStrategy.first
}